import express from 'express';
import cors from 'cors';

const app = express();

app.use(cors());

app.get('/', function (req, res) {
  res.send('Hi all');
});

// Задача 2A: A + B
app.get('/task2A', function (req, res) {
  res.header('Access-Control-Allow-Origin', '*');
  try {
    const a = req.query.a && isFinite(req.query.a) ? +req.query.a : 0;
    const b = req.query.b && isFinite(req.query.b) ? +req.query.b : 0;

    res.send(`${a + b}`);

  } catch (err) {
    console.log('err', err);
  }
});


/*
 * Задача 2B: A + B
 * Клиент делает GET запрос с параметром Query: ?fullname в виде Имя Отчество Фамилия (или Имя Фамилия или Фамилия),
 * ваша задача сделать вывести инициалы в таком виде: Фамилия И. О. ( или Фамилия И.).
 * Результат нужно вывести в виде строки, .
 * при неверных входных данных нужно вывести слово Invalid fullname.
 *
 */
app.get('/task2b', function (req, res) {
  res.header('Access-Control-Allow-Origin', '*');
  try {
    const fullname = req.query.fullname;

    let fullnameArray = fullname ? fullname.split(' ') : [];

    if (!fullnameArray[0] || fullnameArray.length > 3) {
      res.send('Invalid fullname');
    } else {

      const lastName = fullnameArray[fullnameArray.length - 1];
      fullnameArray.pop();
      const namesArray = [].concat(lastName, fullnameArray);

      const resultArr = namesArray.map((name, index) => {
        if (index == 0) return name;
        return `${name.charAt(0)}.`;
      });
      const result = resultArr.join(' ');

      res.send(result);
    }
  } catch (err) {
    console.log('err', err);
  }
});

/*
 * Задача 2C: @username
 * Верезать из строки вида `telegram.me/skillbranch` username `skillbranch`
 */
app.get('/task2c', function (req, res) {
  try {
    const username = req.query.username;

    const USER_REGEXP = /@?(https?:)?(\/\/)?(.*[\/]*\/)?([a-zA-Z0-9]*)([?|\/].*)?$/;

    const realUsername = username.match(USER_REGEXP)[4];

    res.send(`@${realUsername}`);

  } catch (err) {
    console.log('err', err);
  }
});

function rgbToHex(c) {
  const hex = c.toString(16);
  return hex.length == 1 ? "0" + hex : hex;
}

function rgbObjToHex(rgb) {
  let hex = '#';

  for (let color in rgb) {
    hex += rgbToHex(Math.round(rgb[color]));
  }
  return hex;
}

function hueToRgb(p, q, t) {
  if (t < 0) {
    t += 1;
  }
  if (t > 1) {
    t -= 1;
  }
  if (t < 1 / 6) {
    return p + ( q - p ) * 6 * t;
  }
  if (t < 1 / 2) {
    return q;
  }
  if (t < 2 / 3) {
    return p + ( q - p ) * ( ( 2 / 3 - t ) * 6 );
  }

  return p;
}

function hslToRgb(bits) {
  let rgb = {}, hsl = {
    h: bits[0] / 360,
    s: bits[1] / 100,
    l: bits[2] / 100
  };
  if (hsl.s === 0) {
    let v = 255 * hsl.l;
    rgb = {
      r: v,
      g: v,
      b: v
    };
  } else {
    let q = hsl.l < 0.5 ? hsl.l * ( 1 + hsl.s ) : ( hsl.l + hsl.s ) - ( hsl.l * hsl.s );
    let p = 2 * hsl.l - q;
    rgb.r = hueToRgb(p, q, hsl.h + ( 1 / 3 )) * 255;
    rgb.g = hueToRgb(p, q, hsl.h) * 255;
    rgb.b = hueToRgb(p, q, hsl.h - ( 1 / 3 )) * 255;
  }

  return rgbObjToHex(rgb);
}

/*
 * Задача 2D: @colors
 * Распознать цвет и привести его к #abcdef виду
 */
app.get('/task2d', function (req, res) {
  try {
    let color = req.query.color || '';
    color = color.replace(/\s/g, '');
    color = color.replace(/%20/g, '');

    const hexRegexp = /^#?(([0-9A-F]{6})|([0-9A-F]{3}))$/i;
    const rgbRegexp = /^rgb\(((\d{1,2}|[1][0-9][0-9]|[2][0-5][0-5]),\s?(\d{1,2}|[1][0-9][0-9]|[2][0-5][0-5]),\s?(\d{1,2}|[1][0-9][0-9]|[2][0-5][0-5]))\)$/;
    const hslRegexp = /^hsl\((\d{1,3},\d{1,3}%,\d{1,3}%)\)$/;

    const isHEX = hexRegexp.test(color);
    const isRGB = rgbRegexp.test(color);
    const isHSL = hslRegexp.test(color);

    if (isHEX) {
      color = color.replace('#', '').trim();
      if (color.length === 3) {
        color = [].map.call(color, (char) => `${char}${char}`).join('');
      }
      res.send(`#${color}`.toLowerCase());
    } else if (isRGB) {
      const rgbColorArray = color.match(rgbRegexp)[1].split(',');
      const rgbColor = rgbColorArray.map((item) => rgbToHex(parseInt(item))).join('');
      res.send(`#${rgbColor}`.toLowerCase());
    } else if (isHSL) {
      const hslColorArray = color.match(hslRegexp)[1].replace(/%/g, '').split(',');
      if (hslColorArray[0] > 360 || hslColorArray[1] > 100 || hslColorArray[2] > 100) {
        res.send('Invalid color');
      } else {
        res.send(hslToRgb(hslColorArray));
      }
    } else {
      res.send('Invalid color');
    }
  } catch (err) {
    console.log('err', err);
  }
});

/*
 * Задача 2X: Blackbox
 * Воссоздать черный ящик
 */
app.get('/task2x', function (req, res) {
  let result = 0;

  /*
  * ?i=2 = 236
  *	?i=3 = 3240
  *	?i=4 = 43254
  *	?i=5 = 577368
  *	?i=6 = 7706988
  * */

  try {
    const number = parseInt(req.query.i) || 0;

    if (number >= 0 && number <= 18) {
      switch(number) {
        case 0:
          result = 1;
          break;

        case 1:
          result = 18;
          break;

        case 2:
          result = 243;
          break;

        case 3:
          result = 3240;
          break;

        case 4:
          result = 43254;
          break;

        case 5:
          result = 577368;
          break;

        default:
          result = 0;
      }

      res.send(result.toString());
    }
  } catch (err) {
    console.log('err', err);
  }
});

app.listen(3007, function () {
  console.log('App listening on port 3007!');
});

